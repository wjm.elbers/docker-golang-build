#!/usr/bin/env bash

platforms="linux/amd64"
binary="app"
output="/output"

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -b|--binary)
        binary=$2
        shift
        ;;
    -o|--output)
        output=$2
        shift
        ;;
    -p|--platforms)
        platforms=$2
        shift
        ;;
    *)
        echo "Unkown option: $key"
        exit 1
        ;;
esac
shift # past argument or value
done

IFS=',' read -ra p <<< "$platforms"
for i in "${p[@]}"; do
  IFS='/' read -ra x <<< "$i"
  export GOOS=${x[0]} GOARCH=${x[1]}
  echo "Building binary for GOOS=${GOOS} GOARCH=${GOARCH}"
  go build -a -ldflags="-s -w" -o "${output}/${binary}_${GOOS}_${GOARCH}" *.go
done